//
//  NewGame.m
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/9.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <Foundation/Foundation.h>



#import "NewGame.h"

@implementation NewGame
@synthesize directions;
@synthesize tiles;



int marker[9];

int cr = 3;
int mx;
int my;


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
    
        tiles = [[NSMutableArray alloc] init];
        CGRect bounds = [self bounds];
        float w = bounds.size.width/3;
        float h = bounds.size.height/cr;
        for (int r = 0; r < cr; ++r)
        {
            for (int c = 0; c < 3; ++c)
            {
                TileView *tv = [[TileView alloc] initWithFrame:CGRectMake(c*w + 4, r*h + 4, w - 4, h - 4)];
                [tiles addObject:tv];
                [tv setValue:0];
                [self addSubview:tv];
            }
        }
        
    }
    
    for(int i = 0; i < cr; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            marker[i*3 + j] = 0;
        }
    }
   [self initial];
    return self;
}




-(void)initial
{
    int j;
    //NSLog(@"SB");
    for(int i = 0; i < cr; i++)
    {
        for(j = 0; j < 3; j++)
        {
            marker[i*3 + j] = -1;
        }
    }
    
    marker[0] = 1;     marker[1] = 0;      marker[2] = 3;
    
        marker[3] = 7;     marker[4] = 4;     marker[5] = 2;     marker[6] = 8;     marker[7] = 5;     marker[8] = 6;
    int v = 0, i;
    for (j = 0; j < cr * 3; ++j)
    {
        TileView *tv = [tiles objectAtIndex:j];
//        if(j!=2){
//        int k =1;
//        while(k ==1)
//        {
//        v = 1 + rand()%8;
//            
//                for(i = 0; i < 9; i++)
//                    if(v == marker[i] ) break;
//            if(i ==9 ) k = 0;
//                        
//        }
//        
//            marker[j] = v;}
//        else
//            marker[j] = 0;
        [tv setValue:marker[j]];
        //NSLog(@"SB");
        //marker[(i + j)%64] = 1;
        // [tv.label setBackgroundColor:[UIColor orangeColor]];
        [tv updateLabel];
        // return;
        
    }
    
    
    mx = 0;
    my = 1;
    
    
}


-(void)up
{
    
    if(mx <2)
    {
        marker[(mx) * 3 + my]  = marker[(mx+1) * 3 + my];
        marker[(mx+1) * 3 + my] = 0;
        mx++;
        [self update];
        [self checkwin];
    }
}



-(void)down
{
    
    if(mx > 0)
    {
        marker[(mx) * 3 + my]  = marker[(mx-1) * 3 + my];
        marker[(mx-1) * 3 + my] = 0;
        mx--;
        [self update];
        [self checkwin];
    }
}

-(void)right
{
    
    if(my > 0)
    {
        marker[(mx) * 3 + my  ]  = marker[(mx) * 3 + my -1];
        marker[(mx) * 3 + my -1] = 0;
        my--;
        [self update];
        [self checkwin];
    }
}


-(void)left
{
    
    if(my < 2)
    {
        marker[(mx) * 3 + my]  = marker[(mx) * 3 + my +1];
        marker[(mx) * 3 + my+1] = 0;
        my++;
        [self update];
        [self checkwin];
    }
}
-(void) update{

    for (int j = 0; j < cr * 3; ++j)
    {
        TileView *tv = [tiles objectAtIndex:j];
        [tv setValue:marker[j]];
        [tv updateLabel];}
    
}

-(void)checkwin{
    int k = 0;
    NSLog(@"wincheck");
    for (int j = 0; j < cr * 3; j++)
    {
        if(marker[j] != j+1) k++;
    }
    if(k <= 1)
    {
        NSLog(@"win,%d", k);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Win!"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Play again"
                                              otherButtonTitles:@"Back",nil];
        [alert show];
        // [_delegate gameover];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self initial];
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        [self initial];
        NSLog(@"OK Tapped. Hello World!");
    }
}

- (IBAction)handleSwipe:(UISwipeGestureRecognizer *)sender
{
    NSLog(@")swipe");
    
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *) sender direction];
    
    switch (direction) {
     
        case UISwipeGestureRecognizerDirectionLeft:
         NSLog(@"Left");
            [self left];
            break;
        case UISwipeGestureRecognizerDirectionRight:
            NSLog(@"Right");
            [self right];
            break;
        case UISwipeGestureRecognizerDirectionUp:
                     NSLog(@"Up");
            [self up];
            break;
        case UISwipeGestureRecognizerDirectionDown:
                                 NSLog(@"down");
            [self down];
            break;
        default:
            break;
    }
}






@end
