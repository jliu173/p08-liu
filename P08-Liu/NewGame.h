//
//  NewGame.h
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/9.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#ifndef NewGame_h
#define NewGame_h

#import <UIKit/UIKit.h>
#import "TileView.h"


@class NewGame;
@protocol NewGameDelegate <NSObject>

@optional

-(void)gameover;
@end

@interface NewGame : UIView
@property (nonatomic, strong) NSArray *directions;
@property (nonatomic, strong) NSMutableArray *tiles;

@end


#endif /* NewGame_h */


