////
////  Game1.m
////  P08-Liu
////
////  Created by 刘江韵 on 2017/5/10.
////  Copyright © 2017年 刘江韵. All rights reserved.
////
//
#import <Foundation/Foundation.h>



#import "Game1.h"


@implementation Game1
{
    int cr;
    int marker[4];
    int mx;
    int my;
    
}

@synthesize directions;
@synthesize tiles;


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        cr =2;
        tiles = [[NSMutableArray alloc] init];
        CGRect bounds = [self bounds];
        float w = bounds.size.width/2;
        float h = bounds.size.height/cr;
        for (int r = 0; r < cr; ++r)
        {
            for (int c = 0; c < 2; ++c)
            {
                Scene *tv = [[Scene alloc] initWithFrame:CGRectMake(c*w + 4, r*h + 4, w - 4, h - 4)];
                [tiles addObject:tv];
                [tv setValue:0];
                [self addSubview:tv];
            }
        }
        
    }
    
    for(int i = 0; i < cr; i++)
    {
        for(int j = 0; j < 2; j++)
        {
            marker[i*2 + j] = 0;
        }
    }
    [self initial];
    return self;
}




-(void)initial
{
    int j;
    //NSLog(@"SB");
    for(int i = 0; i < cr; i++)
    {
        for(j = 0; j < 2; j++)
        {
            marker[i*2 + j] = -1;
        }
    }
    
    int v, i;
    marker[0] = 2;
     marker[1] = 3;
    marker[2]=1;
        marker[3] = 0;
    for (j = 0; j < cr * 2; ++j)
    {
        Scene *tv = [tiles objectAtIndex:j];
        
//        if(j!=2){
//            int k =1;
//            while(k ==1)
//            {
//                v = 1 + rand()%3;
//                
//                for(i = 0; i < 4; i++)
//                    if(v == marker[i] ) break;
//                if(i ==4 ) k = 0;
//                
//            }
//            
//            marker[j] = v;}
//        else
//            marker[j] = 0;
        [tv setValue:marker[j]];
        //NSLog(@"SB");
        //marker[(i + j)%64] = 1;
        // [tv.label setBackgroundColor:[UIColor orangeColor]];
        [tv updateLabel];
        // return;
        
    
        
    }
    
    mx = 1;
    my = 1;
    
    
}

-(void)up
{
    
    if(mx <1)
    {
        marker[(mx) * 2 + my]  = marker[(mx+1) * 2 + my];
        marker[(mx+1) * 2 + my] = 0;
        mx++;
        [self update];
        [self checkwin];
    }
}



-(void)down
{
    
    if(mx > 0)
    {
        marker[(mx) * 2 + my]  = marker[(mx-1) * 2 + my];
        marker[(mx-1) * 2 + my] = 0;
        mx--;
        [self update];
        [self checkwin];
    }
}

-(void)right
{
    
    if(my > 0)
    {
        marker[(mx) * 2 + my  ]  = marker[(mx) * 2 + my -1];
        marker[(mx) * 2 + my -1] = 0;
        my--;
        [self update];
        [self checkwin];
    }
}


-(void)left
{
    
    if(my < 1)
    {
        marker[(mx) * 2 + my]  = marker[(mx) * 2 + my +1];
        marker[(mx) * 2 + my+1] = 0;
        my++;
        [self update];
        [self checkwin];
    }
}
-(void) update{
    
    for (int j = 0; j < cr * 2; ++j)
    {
        Scene *tv = [tiles objectAtIndex:j];
        [tv setValue:marker[j]];
        [tv updateLabel];}
    
}

-(void)checkwin{
    int k = 0;
    NSLog(@"wincheck");
    for (int j = 0; j < cr * 2; j++)
    {
        if(marker[j] != j+1) k++;
    }
    if(k <= 1)
    {
        NSLog(@"win,%d", k);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Win!"
                                                        message:@""
                                                       delegate:self
                                              cancelButtonTitle:@"Play again"
                                              otherButtonTitles:@"Back",nil];
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self initial];
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        [self initial];
        NSLog(@"OK Tapped. Hello World!");
    }
}


- (IBAction)handleSwipe:(UISwipeGestureRecognizer *)sender
{
    NSLog(@")swipe");
    
    UISwipeGestureRecognizerDirection direction = [(UISwipeGestureRecognizer *) sender direction];
    
    switch (direction) {
            
        case UISwipeGestureRecognizerDirectionLeft:
            NSLog(@"Left");
            [self left];
            break;
        case UISwipeGestureRecognizerDirectionRight:
            NSLog(@"Right");
            [self right];
            break;
        case UISwipeGestureRecognizerDirectionUp:
            NSLog(@"Up");
            [self up];
            break;
        case UISwipeGestureRecognizerDirectionDown:
            NSLog(@"down");
            [self down];
            break;
        default:
            break;
    }
}


//


@end
