//
//  GameViewController.h
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/9.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#ifndef GameViewController_h
#define GameViewController_h

#import <UIKit/UIKit.h>
#import "NewGame.h"
@interface GameViewController : UIViewController <NewGameDelegate>
{
    
    int mainInt;
    IBOutlet UILabel *seconds;
   
    NSTimer *timer;
}


-(IBAction)Quit;
-(IBAction)Play;
@end


#endif /* GameViewController_h */
