//
//  Scene.m
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/7.
//  Copyright © 2017年 刘江韵. All rights reserved.
//
#import <Foundation/Foundation.h>


#import "Scene.h"

@implementation Scene
@synthesize label;
@synthesize value;


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:label];
        [label setText:@""];
        [label setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor orangeColor]];
    }
    return self;
}

-(void)updateLabel
{
    if (value == 0)
    {
        [label setText:@""];
        [label setBackgroundColor:[UIColor whiteColor]];
//         NSLog(@"ssssssss");
//                UIGraphicsBeginImageContext(label.frame.size);
//                [[UIImage imageNamed:@"h1.jpg"] drawInRect:label.bounds];
//                UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//                UIGraphicsEndImageContext();
//        
//                label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 1)
    {
        //        [label setBackgroundColor:[UIColor whiteColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"h1.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 2)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"h2.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 3)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"h3.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }


}
@end

