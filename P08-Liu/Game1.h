//
//  Game1.h
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/10.
//  Copyright © 2017年 刘江韵. All rights reserved.
//


#ifndef Game1_h
#define Game1_h

#import <UIKit/UIKit.h>
#import "Scene.h"

@interface Game1 : UIView
@property (nonatomic, strong) NSArray *directions;
@property (nonatomic, strong) NSMutableArray *tiles;

@end


#endif /* Game1_h */
