//
//  EasyViewController.m
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/10.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "EasyViewController.h"

@interface EasyViewController ()

@end

@implementation EasyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mainInt1 = 30;
    timer1 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
    
    // Do any additional setup after loading the view, typically from a nib.
    //self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"back.jpeg"] ];
    
    
    
    //    UISwipeGestureRecognizer *recognizer;
    //
    //    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    //    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    //    [[self view] addGestureRecognizer:recognizer];
    //
    //    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    //    [recognizer setDirection:(UISwipeGestureRecognizerDirectionUp)];
    //    [[self view] addGestureRecognizer:recognizer];
    //
    //    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    //    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
    //    [[self view] addGestureRecognizer:recognizer];
    //
    //    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    //    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    //    [[self view] addGestureRecognizer:recognizer];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)restartButtonPress {
//    //  GameViewController *controller = [[GameViewController alloc] initWithNibName:@"GameViewController" bundle:nil];
//    //  [self presentModalViewController:controller animated:NO];
//
//    [self.view setNeedsDisplay];
//}


//-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [[event allTouches] anyObject];
//    CGPoint location = [touch locationInView:touch.view];
//    NSLog(@"Point - %f, %f", location.x, location.y);
//}

-(IBAction)Quit1{
    [timer1 invalidate];
    
}

-(IBAction)Play1{
    [timer1 invalidate];
    
}

-(void)countDown{
    mainInt1 -= 1;
    seconds1.text = [NSString stringWithFormat:@"%i", mainInt1];
    if (mainInt1 == 0) {
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"GAME OVER" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertview show];
        
        [timer1 invalidate];
    }
}



@end
