//
//  EasyViewController.h
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/10.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#ifndef GameViewController_h
#define GameViewController_h

#import <UIKit/UIKit.h>

@interface EasyViewController : UIViewController{
    
    int mainInt1;
    
    IBOutlet UILabel *seconds1;
    
    NSTimer *timer1;
}

-(IBAction)Quit1;
-(IBAction)Play1;


@end


#endif /* GameViewController_h */
