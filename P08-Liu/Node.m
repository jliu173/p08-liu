//
//  Node.m
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/6.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

//#import "Node.h"
//
//@implementation FYScrollingNode
//
//+ (id) createNodeWithImageNamed:(NSString *)name inSizeWidth:(float) width {
//    UIImage * image = [UIImage imageNamed:name];
//    
//   
///**
// 更新精灵位置
// */
//- (void) updatePostion:(NSTimeInterval)time {
//    [self.children enumerateObjectsUsingBlock:^(SKNode * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        SKSpriteNode *child = (SKSpriteNode *)obj;
//        child.position = CGPointMake(child.position.x-self.scrollingSpeed, child.position.y);
//        if (child.position.x <= -child.size.width){
//            float delta = child.position.x+child.size.width;
//            child.position = CGPointMake(child.size.width*(self.children.count-1)+delta, child.position.y);
//        }
//    }];
//}
//
//@end
//
//
//
//
//@interface FYBirdNode ()
//@property (strong,nonatomic) SKAction * flapAction;
//@property (strong,nonatomic) SKAction * flapActionForever;
//@end
//
//@implementation FYBirdNode
//
//static CGFloat deltaPosY = 0;
//static bool goingUp = false;
//
//- (id)init {
//    if(self = [super init]) {
//        
//        // 总共3个状态，标示飞行中
//        SKTexture* birdTexture1 = [SKTexture textureWithImageNamed:@"bird_1"];
//        birdTexture1.filteringMode = SKTextureFilteringNearest;
//        SKTexture* birdTexture2 = [SKTexture textureWithImageNamed:@"bird_2"];
//        birdTexture2.filteringMode = SKTextureFilteringNearest;
//        SKTexture* birdTexture3 = [SKTexture textureWithImageNamed:@"bird_3"];
//        birdTexture3.filteringMode = SKTextureFilteringNearest;
//        
//        self = [FYBirdNode spriteNodeWithTexture:birdTexture1];
//        
//        self.flapAction = [SKAction animateWithTextures:@[birdTexture1, birdTexture2, birdTexture3] timePerFrame:0.2];
//        self.flapActionForever = [SKAction repeatActionForever:self.flapAction];
//        
//        [self setTexture:birdTexture1];
//        [self runAction:self.flapActionForever withKey:@"flapActionForever"];
//    }
//    return self;
//}
//
//- (void) updateState:(NSUInteger) time {
//    if(!self.physicsBody){
//        if(deltaPosY > 5.0) {
//            goingUp = false;
//        }
//        if(deltaPosY < -5.0) {
//            goingUp = true;
//        }
//        
//        float displacement = (goingUp)? 1 : -1;
//        self.position = CGPointMake(self.position.x, self.position.y + displacement);
//        deltaPosY += displacement;
//    }
//    self.zRotation = M_PI * self.physicsBody.velocity.dy * 0.0005;
//    
//}
//
//    
//    - (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//        
//        if(!misused) {
//            if (!mybird.physicsBody) {
//                [mybird startFlyBird];
//                [[NSNotificationCenter defaultCenter] postNotificationName:kRetoPlayNotification object:nil];
//            }
//            [mybird jump];
//            
//        } else {
//            [self startPlayGame];
//        }
//    }
//    
//    
//- (void) startFlyBird
//{
//    deltaPosY = 0;
//    [self setPhysicsBody:[SKPhysicsBody bodyWithRectangleOfSize:CGSizeMake(26, 18)]];
//    self.physicsBody.categoryBitMask = birdSpritBitMask;
//    self.physicsBody.mass = 0.1;
//    [self removeActionForKey:@"flapActionForever"];
//}
//
//- (void) jump
//{
//    [self.physicsBody setVelocity:CGVectorMake(0, 0)];
//    [self.physicsBody applyImpulse:CGVectorMake(0, 40)];
//    [self runAction:self.flapAction];
//}
//
//@end

