//
//  TileView.h
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/9.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#ifndef TileView_h
#define TileView_h

#import <UIKit/UIKit.h>

@interface TileView : UIView
@property (nonatomic, strong) UILabel *label;
@property (nonatomic) int value;

-(void)updateLabel;
@end


#endif /* TileView_h */
