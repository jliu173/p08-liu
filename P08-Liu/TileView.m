//
//  TileView.m
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/8.
//  Copyright © 2017年 刘江韵. All rights reserved.
//
//



#import <Foundation/Foundation.h>


#import "TileView.h"

@implementation TileView
@synthesize label;
@synthesize value;


-(id)initWithFrame:(CGRect)frame
{
    
    self = [super initWithFrame:frame];
    if (self)
    {
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:label];
        [label setText:@""];
        [label setTextAlignment:NSTextAlignmentCenter];
        [self setBackgroundColor:[UIColor orangeColor]];
    }
    return self;
}

-(void)updateLabel
{
    if (value == 0)
    {
        [label setText:@""];
        [label setBackgroundColor:[UIColor whiteColor]];
        // NSLog(@"ssssssss");
//        UIGraphicsBeginImageContext(label.frame.size);
//        [[UIImage imageNamed:@"tutu1.jpg"] drawInRect:label.bounds];
//        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
//        
//        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 1)
    {
        //        [label setBackgroundColor:[UIColor whiteColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu1.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 2)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu2.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 3)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu3.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 4)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu4.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 5)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu5.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 6)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu6.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 7)
    {
        //[label setBackgroundColor:[UIColor blackColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu7.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
    else if(value == 8)
    {
       // [label setBackgroundColor:[UIColor whiteColor]];
        
        UIGraphicsBeginImageContext(label.frame.size);
        [[UIImage imageNamed:@"tutu8.jpg"] drawInRect:label.bounds];
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        label.backgroundColor = [UIColor colorWithPatternImage:image];
    }
}
@end

