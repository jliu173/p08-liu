//
//  Scene.h
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/7.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#ifndef Scene_h
#define Scene_h

#import <UIKit/UIKit.h>

@interface Scene : UIView
@property (nonatomic, strong) UILabel *label;
@property (nonatomic) int value;

-(void)updateLabel;
@end


#endif /* Scene_h */
