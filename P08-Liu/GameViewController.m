//
//  GameViewController.m
//  P08-Liu
//
//  Created by 刘江韵 on 2017/5/9.
//  Copyright © 2017年 刘江韵. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameViewController.h"
#import "NewGame.h"

@interface GameViewController () <NewGameDelegate>

@end

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    mainInt = 60;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
    
    // Do any additional setup after loading the view, typically from a nib.
    //self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"back.jpeg"] ];
    
    
    
//    UISwipeGestureRecognizer *recognizer;
//    
//    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
//    [recognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
//    [[self view] addGestureRecognizer:recognizer];
//    
//    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
//    [recognizer setDirection:(UISwipeGestureRecognizerDirectionUp)];
//    [[self view] addGestureRecognizer:recognizer];
//    
//    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
//    [recognizer setDirection:(UISwipeGestureRecognizerDirectionDown)];
//    [[self view] addGestureRecognizer:recognizer];
//    
//    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
//    [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
//    [[self view] addGestureRecognizer:recognizer];
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (IBAction)restartButtonPress {
//    //  GameViewController *controller = [[GameViewController alloc] initWithNibName:@"GameViewController" bundle:nil];
//    //  [self presentModalViewController:controller animated:NO];
//
//    [self.view setNeedsDisplay];
//}


//-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch = [[event allTouches] anyObject];
//    CGPoint location = [touch locationInView:touch.view];
//    NSLog(@"Point - %f, %f", location.x, location.y);
//}

-(IBAction)Quit{
    [timer invalidate];
    
}

-(IBAction)Play{
    [timer invalidate];
    
}

-(void)countDown{
    mainInt -= 1;
    seconds.text = [NSString stringWithFormat:@"%i", mainInt];
    if (mainInt == 0) {
        
        UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"GAME OVER" message:nil delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [alertview show];
        
        [timer invalidate];
    }
}

-(void)gameover
{

}

@end









